import {EditorState, basicSetup} from '@codemirror/basic-setup';
import { defaultTabBinding } from '@codemirror/commands';
import {EditorView, keymap} from '@codemirror/view';
import {json} from '@codemirror/lang-json';

const jsonRequestBody = document.querySelector("[data-json-request-body]");
const jsonResponseBody = document.querySelector("[data-json-response-body]");

const basicExtensions = [
    basicSetup,
    keymap.of([defaultTabBinding]),
    json(),
    EditorState.tabSize.of(2)
]

export const requestEditor = new EditorView({
    state: EditorState.create({
            doc: '{\n\t\n}',
            extensions: basicExtensions
        }),
    parent: jsonRequestBody
})

export const responseEditor = new EditorView({
    state: EditorState.create({
        doc: '{}',
        extensions: [...basicExtensions, EditorView.editable.of(false)]
    }),

    parent: jsonResponseBody
})

export const updateResponseEditor = value => {
    responseEditor.dispatch({
        changes:{
            from: 0,
            to: responseEditor.state.doc.length,
            insert: JSON.stringify(value, null, 2)
        }
    })
}

export const updateRequestEditor = value => {
    requestEditor.dispatch({
        changes: {
            from: 0, 
            to: requestEditor.state.doc.length, 
            insert: JSON.stringify(value,null,2)
        }
    })
}
