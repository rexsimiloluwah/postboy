import axios from "axios";
import prettyBytes from "pretty-bytes";
import {requestEditor,updateResponseEditor, updateRequestEditor} from "./setupEditor";
import "../scss/style.scss";

const queryParamsContainer = document.querySelector('[data-query-params]');
const requestHeadersContainer = document.querySelector('[data-request-headers]');
const keyValueTemplate = document.querySelector('[data-key-value-template]');
const addQueryParamField = document.querySelector('[data-add-query-params-btn]');
const addRequestHeaderField = document.querySelector('[data-add-request-headers-btn]');
const requestURLInput = document.querySelector("[data-url]");
const requestMethodSelect = document.querySelector("[data-method]");
const responseHeadersContainer = document.querySelector('.response__content#headers div');
const mainDataForm = document.querySelector('[data-form]');

mainDataForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const submitBtn = mainDataForm.querySelector("button")
    const currentBtnText = submitBtn.innerHTML;
    submitBtn.innerText = "Sending..."
    let data;
    // parse the JSON body
    try{
        data = JSON.parse(requestEditor.state.doc.toString() || null);
    }catch(e){
        alert("JSON data is incorrect");
        return;
    }

    const queryParams = keyValuePairsToObject(queryParamsContainer);
    const headers = keyValuePairsToObject(requestHeadersContainer);
    const url = requestURLInput.value; 
    const method = requestMethodSelect.value;

    // storing request parameters 
    if(data){
        if (Object.keys(data).length > 0) {
          localStorage.setItem("body", JSON.stringify(data));
        }
    }
    localStorage.setItem("queryParams",JSON.stringify(queryParams))
    localStorage.setItem("headers", JSON.stringify(headers))
    localStorage.setItem("url", url);
    localStorage.setItem("method",method);

    // send the request to the endpoint
    axios({
      url: url,
      method: method,
      params: queryParams,
      headers: headers,
      data: data,
    })
      .then((response) => {
        document.querySelector("[data-response-section]").style.display =
          "block";
        document.querySelector("[data-response-placeholder]").style.display =
          "none";
        document.querySelector(
        "[data-response-network-error]"
        ).style.display = "none";
        // update the response content tab
        updateResponseDetails(response); // response status and time
        updateResponseEditor(response.data); // response body
        updateResponseHeaders(response.headers); // response header
        submitBtn.innerHTML = currentBtnText
      })
      .catch((error) => {
        if (!error.response) {
          // handle network error
          document.querySelector("[data-response-placeholder]").style.display =
            "none";
          document.querySelector(
            "[data-response-network-error]"
          ).style.display = "block";
          document.querySelector("[data-response-section]").style.display =
            "none";
          submitBtn.innerHTML = currentBtnText;
        }
      });
})

// update the time elapsed between request and response
const updateEndTime = (response) => {
    response.customData = response.customData || {};
    response.customData.time = new Date().getTime() - response.config.customData.startTime;
    return response;
}

// Interceptors to compute time elapsed for a request 
axios.interceptors.request.use(request => {
    request.customData = request.customData || {};
    request.customData.startTime = new Date().getTime();
    return request;
})

axios.interceptors.response.use(updateEndTime, e => {
    return Promise.reject(updateEndTime(e.response));
})

// Update the response details tab
const updateResponseDetails = (response) => {
    document.querySelector('[data-status]').textContent = response.status;
    document.querySelector('[data-time-elapsed]').textContent = `${response.customData.time} ms`;
    document.querySelector('[data-size]').textContent = prettyBytes(
        JSON.stringify(response.data).length + JSON.stringify(response.headers).length
    );
}

// Update the response headers tab
const updateResponseHeaders = (headers) => {
    responseHeadersContainer.innerHTML = "";
    Object.entries(headers).forEach(([key, value])=> {
        const keyElement = document.createElement('div');
        keyElement.textContent = `${key} : `;
        responseHeadersContainer.append(keyElement);

        const valueElement = document.createElement('div');
        valueElement.textContent = value;
        responseHeadersContainer.append(valueElement);
    } )
}

// Create a Key-Value pair field
const createKeyValuePair = (key,value) => {
    const element = keyValueTemplate.content.cloneNode(true);
    element.querySelector('[data-key]').value = key;
    element.querySelector('[data-value]').value = value;
    element.querySelector('[data-remove-btn]').addEventListener('click', (e)=>{
        e.target.closest('[data-key-value-pair]').remove()
    })

    return element;
}

// convert the data in a key-value input group to an object
const keyValuePairsToObject = (container) => {
    const pairs = container.querySelectorAll('.input__group');
    return [...pairs].reduce((data, pair) => {
        let key = pair.querySelector('[data-key]').value;
        let value = pair.querySelector('[data-value]').value;

        if(key === '') return data;

        return {...data, [key]:value};
    }, {})
}

// Add a new field
addQueryParamField.addEventListener('click', (e)=>{
    queryParamsContainer.append(createKeyValuePair("",""));
})

addRequestHeaderField.addEventListener('click', (e)=>{
    requestHeadersContainer.append(createKeyValuePair("",""));
})


const openTabHeader = (event, tabName)  => {
    let tabLinks = document.querySelectorAll('.nav__tabs .tablink');
    let tabContents = document.querySelectorAll('.tab__content')

    for (const el in tabLinks){
        if (tabLinks[el].classList && tabLinks[el].classList.contains('active')){
            tabLinks[el].classList.remove('active');
        }
    }

    for (const el in tabContents){
        if (tabContents[el].classList && tabContents[el].classList.contains('active')){
            tabContents[el].classList.remove('active');
        }
    }

    document.querySelector(`.tab__content#${tabName}`).classList.add('active');
    event.currentTarget.firstElementChild.classList.add('active');
}

const openTabResponse = (event, tabName) => {
    let tabLinks = document.querySelectorAll('.response__tabs .tablink');
    let responseContents = document.querySelectorAll('.response__content');

    for (const el in tabLinks){
        if (tabLinks[el].classList && tabLinks[el].classList.contains('active')){
            tabLinks[el].classList.remove('active');
        }
    }

    for (const el in responseContents){
        if (responseContents[el].classList && responseContents[el].classList.contains('active')){
            responseContents[el].classList.remove('active');
        }
    }

    document.querySelector(`.response__content#${tabName}`).classList.add('active');
    event.currentTarget.firstElementChild.classList.add('active');
}

const headerTabs = ['queryParams','headers','body','authorization','settings']
const responseTabs = ['body', 'headers'];

const headerTabsLinks = document.querySelectorAll('.nav__tabs a');
const responseTabsLinks = document.querySelectorAll('.response__tabs a');
for(const el in headerTabsLinks){
    if(el>=0){
        headerTabsLinks[el].addEventListener('click', e=> {openTabHeader(e, headerTabs[el])});
    }
}

for(const el in responseTabsLinks){
    if(el>=0){
        responseTabsLinks[el].addEventListener('click', e=> {openTabResponse(e, responseTabs[el])});
    }
}

window.onload = () => {
  const bodyHistory = localStorage.getItem("body");
  const headersHistory = localStorage.getItem("headers");
  const queryParamsHistory = localStorage.getItem("queryParams");
  const url = localStorage.getItem("url")
  const method = localStorage.getItem("method")
  if(url){
    requestURLInput.value = localStorage.getItem("url");
  }
  if(method){
    requestMethodSelect.value = localStorage.getItem("method");
  }
  
  if(headersHistory){
    const headers = JSON.parse(headersHistory);
    Object.keys(headers).map((key) => {
      requestHeadersContainer.append(createKeyValuePair(key, headers[key]));
    });
  }

  if(queryParamsHistory){
    const queryParams = JSON.parse(queryParamsHistory);
    Object.keys(queryParams).map((key) => {
      queryParamsContainer.append(createKeyValuePair(key, queryParams[key]));
    });
  }

  // Add the element to the query params container
  queryParamsContainer.append(createKeyValuePair("", ""));
  requestHeadersContainer.append(createKeyValuePair("", ""));

  // update the body editor
  updateRequestEditor(JSON.parse(bodyHistory));
};;
